#!/usr/bin/env python
# -*- coding: utf-8 -*-

from logging import basicConfig, getLogger, INFO, DEBUG, StreamHandler, Formatter
from typing import Dict, AnyStr
from os import environ
from sys import stdout
from re import sub


from telegram import Bot
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from emoji import emojize
from flair.data import Sentence
from flair.models import SequenceTagger


class EmojifyBot:
    def __init__(self, token: AnyStr):
        basicConfig(level=DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    stream=stdout)

        self.logger = getLogger('bot')
        self.logger.setLevel(INFO)
        # self.logger.setFormatter(Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))

        # log_handler = StreamHandler(stdout)
        # log_handler.setLevel(INFO)
        # log_handler.setFormatter
        # self.logger.addHandler(log_handler)

        self.logger.info('starting bot')

        self.token = token
        assert len(self.token) > 0, "token too short"

        self.bot = Bot(token=self.token)
        self.updater = Updater(self.token, use_context=True)
        self.dispatcher = self.updater.dispatcher

        self.logger.info('loading model')
        self.model = SequenceTagger.load_from_file("model.pt")
        self.logger.info('loaded model')

        self.handlers: Dict = dict()
        self.handlers['start'] = CommandHandler('start', self.start_handler)
        self.handlers['info'] = CommandHandler('info', self.info_handler)
        self.handlers['creators'] = CommandHandler(
            'creators', self.creators_handler)
        self.handlers['any_text'] = MessageHandler(
            Filters.text, self.any_text_handler)

        for handler in self.handlers:
            self.dispatcher.add_handler(self.handlers[handler])

        self.logger.info('started bot')

    def start_handler(self, update, context):
        self.logger.info(f'{update.message.chat.username} started bot')
        with open('usernames', 'a+') as file:
            file.write(update.message.chat.username+'\n')
        context.bot.send_message(chat_id=update.message.chat_id,
                                 text="Send me any text and I'll try to add emoji to it")

    def info_handler(self, update, context):
        self.logger.info(
            f'{update.message.chat.username} requested information about the bot')
        context.bot.send_message(chat_id=update.message.chat_id,
                                 parse_mode="Markdown",
                                 text="A simple bot, written with [python-telegram-bot]("
                                      "https://github.com/python-telegram-bot/python-telegram-bot)."
                                      #"Text is processed on a server on [Flask](https://github.com/pallets/flask)"
                                 )

    def creators_handler(self, update, context):
        self.logger.info(
            f'{update.message.chat.username} requested information about creators')
        context.bot.send_message(chat_id=update.message.chat_id,
                                 parse_mode="Markdown",
                                 text="@RandomDanil & @Daniil, "
                                      "source code available at "
                                      "[GitLab](https://gitlab.com/randomunrandom/emojify_bot)")

    def any_text_handler(self, update, context):
        with open('usernames', 'a+') as file:
            file.write(update.message.chat.username+'\n')
        start_text = update.message.text
        modified_text = Sentence(start_text)
        self.model.predict(modified_text)
        modified_text = modified_text.to_tagged_string()
        # self.logger.debug(text)
        modified_text = emojize(modified_text)
        modified_text = sub('\<|\>', '', modified_text)
        send_text = f'Sorry, I don\'t think I should add anything to "{start_text}"' if modified_text == start_text else modified_text
        context.bot.send_message(chat_id=update.message.chat_id,
                                 text=send_text)
        log = f'@{update.message.chat.username} wrote "{update.message.text}" and I '
        log = log + 'didn\'t add anythng' if modified_text == start_text else log + \
            f'answered "{modified_text}"'
        self.logger.info(log)

    def start(self):
        self.logger.info('start polling')
        self.updater.start_polling()


if __name__ == '__main__':
    token: AnyStr = environ.get("BOT_TOKEN", "")
    bot = EmojifyBot(token)
    bot.start()
