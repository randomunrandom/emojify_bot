FROM python:3.7-slim

ENV PIPENV_VENV_IN_PROJECT=1
ENV BOT_TOKEN ""

WORKDIR /bot
ADD requirements.txt .

RUN apt-get update
RUN apt-get install -y wget
RUN apt-get clean
RUN pip3 install -r requirements.txt --no-cache-dir

ADD model.pt .
# RUN wget https://randomunrandom.keybase.pub/final-model.pt -O model.pt -nv

ADD main.py .


CMD pipenv run python3 main.py
